<h1 align="center">Projet Activite Professionnelle: Application mobile de gestion de produits pour l'Association Sportive de la Maison des Ligues de Lorraine</h1>

<h2>Description</h2>

Ce projet est une activité professionnelle réalisée dans le cadre de mon BTS SIO à l'IPSSI. L'objectif est de développer 
une application mobile en utilisant Flutter pour le front-end et Node.js pour le back-end, destinée à
l'association sportive de la Maison des Ligues de Lorraine. L'application permettra aux membres de l'association de
gérer les produits liés à leurs activités sportives.
<h2>Fonctionnalites</h2>

- Interface administrateur permettant aux membres de l'association de parcourir les produits, de les créer, de les modifier et de les supprimer.
- Système d'authentification sécurisé pour les administrateur.

<h2>Technologies Utilisées</h2>

- **Front-end** : Flutter
- **Back-end** : Node.js, Express.js, MySQL
- **Authentification et Sécurité** : JSON Web Tokens (JWT), Bcrypt.js
- **Autres Outils** : Git/GitHub pour le contrôle de version, dotenv pour la gestion des variables d'environnement