import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ap4/homePage.dart';
import 'package:ap4/editForm.dart';
import 'package:ap4/addForm.dart';
import 'package:ap4/auth_Token.dart';

void main() {
  runApp(EditFormApp());
}

class EditFormApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: EditForm(productDetails: {
        'nom': 'Nom du produit',
        'prix': 0.0,
        'description': 'Description du produit',
        'stock': 0,
        'id': '123',
      }),
      routes: {
        '/home': (context) =>
            const HomePage(title: 'La Maison des Ligues de Lorraine'),
        '/addForm': (context) => AddFormApp(),
        '/editForm': (context) => EditFormApp(),
      },
    );
  }
}

class EditForm extends StatelessWidget {
  final Map<String, dynamic> productDetails;
  final TextEditingController nomController = TextEditingController();
  final TextEditingController prixController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController stockController = TextEditingController();
  final TextEditingController imageController = TextEditingController();

  EditForm({Key? key, required this.productDetails}) : super(key: key) {
    nomController.text = productDetails['nom'].toString();
    prixController.text = productDetails['prix'].toString();
    descriptionController.text = productDetails['description'].toString();
    stockController.text = productDetails['stock'].toString();
    imageController.text = productDetails['image'].toString();
  }

  @override
  Widget build(BuildContext context) {
    String imageUrl = productDetails['image'] != null
        ? 'http://localhost:8000/images/${productDetails['image']}'
        : 'http://localhost:8000/images/no_pic.png';
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFEBB717),
        toolbarHeight: 150,
        title: Row(
          children: [
            Image.asset(
              'assets/Logo_M2L_sans_titre.png',
              height: 70,
            ),
            const SizedBox(width: 20),
            Text(
              'Modifier un produit',
              style: const TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // Affichage de l'image du produit
            Image.network(
              imageUrl,
              height: 150,
              width: 150,
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: nomController,
              decoration: InputDecoration(
                labelText: 'Nom',
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: prixController,
              decoration: InputDecoration(
                labelText: 'Prix',
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: 'Description',
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: stockController,
              decoration: InputDecoration(
                labelText: 'Stock',
              ),
              keyboardType: TextInputType.number,
            ),
            TextButton(
              onPressed: () async {
                String nom = nomController.text;
                double prix = double.tryParse(prixController.text) ?? 0.0;
                String description = descriptionController.text;
                int stock = int.tryParse(stockController.text) ?? 0;
                String image = imageController.text;

                Map<String, dynamic> modifiedProduct = {
                  'nom': nom,
                  'prix': prix,
                  'description': description,
                  'stock': stock,
                  'image': image,
                };

                String jsonBody = jsonEncode(modifiedProduct);

                http.put(
                  Uri.parse(
                      'http://localhost:8000/api/produit/editProduct/${productDetails['id']}'),
                  body: jsonBody,
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': '${Auth_Token.token}',
                  },
                ).then((response) {
                  if (response.statusCode == 200) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Le produit a été modifié avec succès'),
                      ),
                    );
                    Navigator.pop(context);
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('La modification du produit a échoué'),
                      ),
                    );
                  }
                }).catchError((error) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Une erreur s\'est produite: $error'),
                    ),
                  );
                });
              },
              child: Text('Modifier le produit'),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Retour'),
            )
          ],
        ),
      ),
    );
  }
}
