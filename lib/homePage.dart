import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ap4/addForm.dart';
import 'package:ap4/editForm.dart';
import 'package:ap4/auth_Token.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

// GET products
Future<List<Map<String, dynamic>>> recupererProduits() async {
  final url =
      Uri.parse('http://localhost:8000/api/produit/allProducts'); //10.74.1.203

  try {
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body);
      return data.cast<Map<String, dynamic>>();
    } else {
      throw Exception('Failed to load products');
    }
  } catch (e) {
    throw Exception('Failed to load products: $e');
  }
}

class _HomePageState extends State<HomePage> {
// GET products
  List<Map<String, dynamic>> products = [];
  @override
  void initState() {
    super.initState();
    _loadProducts(); // Appel à la méthode pour charger les produits
  }

  // Méthode pour charger les produits
  void _loadProducts() {
    recupererProduits().then((List<Map<String, dynamic>> produits) {
      setState(() {
        products.clear();
        products.addAll(produits);
      });
    }).catchError((error) {
      print('Error loading products: $error');
    });
  }

// DELETE product
  Future<void> deleteProduct(String id) async {
    final url = Uri.parse(
      'http://localhost:8000/api/produit/deleteProduct/$id',
    );

    try {
      final response = await http.delete(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': '${Auth_Token.token}',
        },
      );

      if (response.statusCode == 200 || response.statusCode == 204) {
        // Produit supprimé avec succès, actualisez l'affichage des produits
        setState(() {
          products.removeWhere((product) => product['id'] == id);
        });

        // Actualisez les produits après avoir supprimé le produit
        _loadProducts();
      } else {
        throw Exception('Failed to delete product');
      }
    } catch (e) {
      print('Error deleting product: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    // Vérifie si le token est vide
    if (Auth_Token.token!.isEmpty) {
      // Redirige vers l'écran de connexion
      Navigator.pushReplacementNamed(context, '/');
      // Retourne un conteneur vide en attendant la redirection
      return Container();
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFEBB717),
        toolbarHeight: 150,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  'assets/Logo_M2L_sans_titre.png',
                  height: 80,
                  width: 80,
                ),
                const SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'La Maison des',
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Ligues de Lorraine',
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
                height: 10), // Espace vertical entre le titre et le bouton
            ElevatedButton(
              onPressed: () {
                // Suppression du token lors de la déconnexion
                Auth_Token.removeToken();
                // Retour à l'écran de connexion
                Navigator.pushReplacementNamed(context, '/');
              },
              child: const Text('Déconnexion'),
            ),
          ],
        ),
      ),
      body: Container(
        color: Colors.grey[200],
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        AddFormHomePage(title: 'Ajouter un produit'),
                  ),
                ).then((_) {
                  // Actualiser les produits après avoir ajouté un produit
                  _loadProducts();
                });
              },
              child: const Text('Ajouter'),
            ),
            const SizedBox(height: 16.0),
            Expanded(
              child: ListView.builder(
                itemCount: products.length,
                itemBuilder: (context, index) {
                  final product = products[index];
                  return Card(
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    child: ListTile(
                      title: Text(product['nom'] ?? ''),
                      subtitle: Text(
                        'Prix: ${product['prix']?.toString() ?? ''}, Stock: ${product['stock']?.toString() ?? ''}',
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.edit),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      EditForm(productDetails: product),
                                ),
                              ).then((_) {
                                // Actualiser les produits après avoir modifié un produit
                                _loadProducts();
                              });
                            },
                          ),
                          IconButton(
                            icon: const Icon(Icons.delete),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Confirmation"),
                                    content: Text(
                                        "Voulez-vous vraiment supprimer ce produit ?"),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text("Annuler"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                      TextButton(
                                        child: Text("Supprimer"),
                                        onPressed: () {
                                          deleteProduct(product['id']);
                                          Navigator.of(context).pop();
                                          print('Produit supprimé');
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
