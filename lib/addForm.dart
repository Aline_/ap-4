import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ap4/homePage.dart';
import 'dart:convert';
import 'auth_Token.dart';

void main() {
  runApp(AddFormApp());
}

class AddFormApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AddFormHomePage(title: 'Ajouter un produit'),
      routes: {
        '/home': (context) =>
            const HomePage(title: 'La Maison des Ligues de Lorraine'),
      },
    );
  }
}

class AddFormHomePage extends StatefulWidget {
  final String title;

  const AddFormHomePage({Key? key, required this.title}) : super(key: key);

  @override
  _AddFormHomePageState createState() => _AddFormHomePageState();
}

class _AddFormHomePageState extends State<AddFormHomePage> {
  final TextEditingController nomController = TextEditingController();
  final TextEditingController prixController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController stockController = TextEditingController();

  Future<void> addProduct(String nom, double prix, String description,
      int stock, BuildContext context) async {
    final url = Uri.parse('http://localhost:8000/api/produit/addProduct');

    try {
      var response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': '${Auth_Token.token}',
        },
        body: jsonEncode({
          'nom': nom,
          'prix': prix,
          'description': description,
          'stock': stock,
        }),
      );
      if (response.statusCode == 200) {
        Navigator.pop(context);
      } else {
        throw Exception('Failed to add product');
      }
    } catch (e) {
      print('Error adding product: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFEBB717),
        toolbarHeight: 150,
        title: Row(
          children: [
            const SizedBox(width: 20),
            Text(
              widget.title,
              style: const TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                controller: nomController,
                decoration: InputDecoration(
                  labelText: 'Nom',
                ),
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: prixController,
                decoration: InputDecoration(
                  labelText: 'Prix',
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  labelText: 'Description',
                ),
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: stockController,
                decoration: InputDecoration(
                  labelText: 'Stock',
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      String nom = nomController.text;
                      double prix = double.tryParse(prixController.text) ?? 0.0;
                      String description = descriptionController.text;
                      int stock = int.tryParse(stockController.text) ?? 0;

                      if (nom.isNotEmpty &&
                          prix > 0 &&
                          description.isNotEmpty &&
                          stock > 0) {
                        addProduct(nom, prix, description, stock, context);
                      } else {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Champs invalides'),
                              content:
                                  Text('Veuillez remplir tous les champs.'),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('OK'),
                                ),
                              ],
                            );
                          },
                        );
                      }
                    },
                    child: Text('Ajouter le produit'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Retour'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
