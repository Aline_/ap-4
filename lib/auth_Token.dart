// Classe qui permet de stocker le token de l'utilisateur connecté
class Auth_Token {
  static String? _token; // Le token d'authentification, initialisé à null

  // Méthode pour récupérer le token
  static String? get token => _token;

  // Méthode pour définir le token
  static void setToken(String? token) {
    _token = token;
  }

  // Méthode pour supprimer le token (déconnexion)
  static void removeToken() {
    _token = null;
  }
}
