import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ap4/homePage.dart';
import 'package:ap4/addForm.dart';
import 'package:ap4/editForm.dart';
import 'package:ap4/auth_Token.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'M2L',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: const Color.fromRGBO(235, 183, 23, 1)),
        useMaterial3: true,
      ),
      routes: {
        '/': (context) =>
            const MyHomePage(title: 'La Maison des Ligues de Lorraine'),
        '/home': (context) =>
            const HomePage(title: 'La Maison des Ligues de Lorraine'),
        '/addForm': (context) => AddFormApp(),
        '/editForm': (context) => EditFormApp(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _isUsernameValid = false;
  bool _isPasswordValid = false;
  String _loginMessage = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFEBB717),
        toolbarHeight: 150,
        title: Row(
          children: [
            Image.asset(
              'assets/Logo_M2L_sans_titre.png',
              height: 70,
              width: 70,
            ),
            const SizedBox(width: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'La Maison des',
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Ligues de Lorraine',
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'Connexion',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 250,
                height: 40,
                child: TextField(
                  controller: _usernameController,
                  decoration: const InputDecoration(
                    labelText: 'Email *',
                    border: OutlineInputBorder(),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _isUsernameValid = _validateEmail(value);
                    });
                  },
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 250,
                height: 40,
                child: TextField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    labelText: 'Mot de passe *',
                    border: OutlineInputBorder(),
                  ),
                  obscureText: true,
                  onChanged: (value) {
                    setState(() {
                      _isPasswordValid = value.isNotEmpty;
                    });
                  },
                ),
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                onPressed: _isUsernameValid && _isPasswordValid ? _login : null,
                child: const Text('Connexion'),
              ),
              const SizedBox(height: 10),
              Text(_loginMessage)
            ],
          ),
        ),
      ),
    );
  }

  bool _validateEmail(String value) {
    final emailRegex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
    return emailRegex.hasMatch(value);
  }

  void _login() async {
    final String username = _usernameController.text;
    final String password = _passwordController.text;

    final response = await http.post(
      Uri.parse('http://localhost:8000/api/utilisateur/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'mail': username,
        'mdp': password,
      }),
    );

    if (response.statusCode == 200) {
      // Connexion réussie, récupérer les données de réponse
      final responseData = jsonDecode(response.body);
      final token = responseData['token'];
      final isAdmin = responseData['isAdmin'];

      if (isAdmin == 1) {
        // L'utilisateur est un administrateur, procédez à la connexion
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Connexion réussie!'),
          ),
        );

        // Stockage du token dans la variable globale
        Auth_Token.setToken(token);

        // Redirection vers la page HomePage après une connexion réussie
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        // L'utilisateur n'est pas un administrateur, afficher un message d'erreur
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
                'Seuls les administrateurs sont autorisés à se connecter.'),
          ),
        );
      }
    } else {
      // Échec de la connexion
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Échec de la connexion'),
        ),
      );
    }
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
